const express = require("express");
const router = express.Router();
const generateAccessToken = require("../auth/generateToken");

/*********************************** '/api/auth' route **************************/
router.get("/auth", (req, res) => {
  var user = { username: "nayan" };

  var token = generateAccessToken(user);
  console.log("hit /api/auth");
  res.statusCode = 200;
  res.send({
    token: token,
  });
});
/********************************************************************************/

module.exports = router;
