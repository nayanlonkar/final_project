const express = require("express");
const router = express.Router();

/******************************** '/api/register' route *************************/

router.get("/register", (req, res) => {
  console.log("hit /api/register");
  res.send("<h1>Register Page</h1>");
});

/********************************************************************************/

module.exports = router;
