const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

/**************************** to read SECRET_TOKEN value ************************/

// get config vars
dotenv.config();

// access config var
process.env.TOKEN_SECRET;

/********************************************************************************/

/************************** function to authenticate token **********************/

function authenticateToken(req, res, next) {
  // Gather the jwt access token from the request header
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (token == null) return res.sendStatus(401); // if there isn't any token

  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    // console.log(err);
    if (err) return res.sendStatus(403);
    // req.user = user;
    next(); // pass the execution off to whatever request the client intended
  });
}

/********************************************************************************/

module.exports = authenticateToken;
