const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");

/**************************** to read SECRET_TOKEN value ************************/

// get config vars
dotenv.config();

// access config var
process.env.TOKEN_SECRET;
/********************************************************************************/

/**************************** function to generate token ************************/

// username is in the form { username: "my cool username" }
function generateAccessToken(username) {
  // expires after half and hour (1800 seconds = 30 minutes)
  return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: "1800s" });
}

/********************************************************************************/

module.exports = generateAccessToken;
