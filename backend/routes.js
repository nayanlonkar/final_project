const express = require("express");
const router = express.Router();

const app = express();

/************************************* routes imports ***************************/
const login_route = require("./api/login");
const register_route = require("./api/register");
/********************************************************************************/

/*************************************** middlewares ****************************/
router.use("/api", login_route);
router.use("/api", register_route);
/********************************************************************************/

app.get("/", (req, res) => {
  res.send("<h1>Root Page</h1>");
});

module.exports = router;
