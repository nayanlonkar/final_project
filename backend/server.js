const express = require("express");
const cors = require("cors");
const routes = require("./routes"); /* all routes are stored here */
const authenticateToken = require("./auth/authenticateToken");

const app = express();

/********************************* middlewares ******************************************/

app.use(cors()); /* for CORS error */
app.use(express.json()); /* express.json() is used for request body parsing */
app.use("/", routes); /* all routes are in routes.js file */

/****************************************************************************************/

/********************************* database connection **********************************/

const MongoClient = require("mongodb").MongoClient;
const mongo_URI = "mongodb://localhost:27017";
const db_name = "final_project";

MongoClient.connect(mongo_URI, function (err, client) {
  if (err) throw err;
  else console.log(">>>> successfully connected to database " + db_name);

  const db = client.db(db_name);

  db.collection("counters")
    .find()
    .toArray(function (err, result) {
      if (err) throw err;
      console.log(result);
    });
});

/****************************************************************************************/

/****************************************************************************************/
app.listen(3001, () => {
  console.log(">>>> Server is listening on port 3001");
});
/****************************************************************************************/
