import React, { useState } from "react";
import "./App.css";

// component import
import Container from "./components/layouts/Container";
import SignIn from "./components/auth/SignIn";

// This is a root component
function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  return <div className="App">{isLoggedIn ? <Container /> : <SignIn />}</div>;
}

export default App;
